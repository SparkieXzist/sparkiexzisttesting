package services;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;

public class ManageHostPageController
{
    @FXML TextField usernameBox,nameBox,blockBox,unblockBox;
    @FXML PasswordField passwordBox, confirmPasswordBox;
    private AdminAccount adminAccount;
    private HostAccount hostAccount;
    private UserAccount userAccount;
    private Room roomAll;
    public void setRoom(Room room) {roomAll = room;}
    public void setAdminAccounts(AdminAccount accounts) { adminAccount = accounts; }
    public void setHostAccounts(HostAccount accounts) { hostAccount = accounts; }
    public void setUserAccounts(UserAccount accounts) { userAccount = accounts;}
    Alert alert;
    @FXML private Label currentAccountLabel;
    @FXML public void initialize()
    {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccountLabel.setText(adminAccount.getName());
            }
        });
        alert = new Alert(Alert.AlertType.NONE);
    }
    public void handleAddBtnOnAction(ActionEvent event) throws IOException
    {
        try {
            Integer.parseInt(nameBox.getText());
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Name can't be a number.");
            alert.show();
        }
        catch (NumberFormatException e)
        {
            if(!hostAccount.checkExist(usernameBox.getText()))
        {
            if(passwordBox.getText().equals(confirmPasswordBox.getText()))
            {
                hostAccount.addAccount(new HostAccount(usernameBox.getText(), passwordBox.getText(), nameBox.getText(), 1));
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setContentText("Add host successful.");
                alert.show();
                Button LoginBtn = (Button) event.getSource();
                Stage stage = (Stage) LoginBtn.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminmainpage.fxml"));
                stage.setScene(new Scene(loader.load(), 1024, 768));
                AdminMainPageController account = loader.getController();
                account.setAdminAccounts(adminAccount);
                account.setHostAccounts(hostAccount);
                account.setUserAccounts(userAccount);
                account.setRoom(roomAll);
                stage.show();
            }
            else
            {
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setContentText("Password doesn't match.");
                alert.show();
            }
        }
        else
        {
            alert.setAlertType(Alert.AlertType.INFORMATION);
            alert.setContentText("Username is already used.");
            alert.show();
        }
        }
//        if(!hostAccount.checkExist(usernameBox.getText()))
//        {
//            hostAccount.addAccount(new HostAccount(usernameBox.getText(), passwordBox.getText(), nameBox.getText(), 1));
//            alert.setAlertType(Alert.AlertType.INFORMATION);
//            alert.setContentText("Add host successful.");
//            alert.show();
//            Button LoginBtn = (Button) event.getSource();
//            Stage stage = (Stage) LoginBtn.getScene().getWindow();
//            FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminmainpage.fxml"));
//            stage.setScene(new Scene(loader.load(), 1024, 768));
//            AdminMainPageController account = loader.getController();
//            account.setAdminAccounts(adminAccount);
//            account.setHostAccounts(hostAccount);
//            account.setUserAccounts(userAccount);
//            account.setRoom(roomAll);
//            stage.show();
//        }
//        else
//        {
//            alert.setAlertType(Alert.AlertType.INFORMATION);
//            alert.setContentText("Username is already used.");
//            alert.show();
//        }

    }
    public void handleBlockBtnOnAction(ActionEvent event) throws IOException
    {
        for(HostAccount acc: hostAccount.getHostAccounts())
        {
            if(acc.getUsername().equals(blockBox.getText()) && acc.getStatus() == 1)
            {
                acc.setStatus(0);
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setContentText("Block successful.");
                alert.show();
                break;
            }
            else if(acc.getUsername().equals(blockBox.getText()) && acc.getStatus() == 0)
            {
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setContentText("Already blocked.");
                alert.show();
                break;
            }
        }
    }
    public void handleUnblockBtnOnAction(ActionEvent event) throws IOException
    {
        for(HostAccount acc: hostAccount.getHostAccounts())
        {
            if(acc.getUsername().equals(unblockBox.getText()) && acc.getStatus() == 0)
            {
                acc.setStatus(1);
                acc.setAttempt(0);
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setContentText("Unblock successful.");
                alert.show();
                break;
            }
            else if(acc.getUsername().equals(unblockBox.getText()) && acc.getStatus() == 1)
            {
                alert.setAlertType(Alert.AlertType.INFORMATION);
                alert.setContentText("This username is not blocked.");
                alert.show();
                break;
            }
        }
    }
    public void handleManageHostBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/managehostpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageHostPageController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
    public void handleHostListBtnOnAction(ActionEvent event) throws IOException
    {
//        System.out.println(accounts.getCurrentAccount());
//        Button LoginBtn = (Button) event.getSource();
//        Stage stage = (Stage) LoginBtn.getScene().getWindow();
//        FXMLLoader loader = new FXMLLoader(getClass().getResource("managehostpage.fxml"));
//        stage.setScene(new Scene(loader.load(), 1024, 768));
//        ManageHostPageController account = loader.getController();
//        account.setAccounts(accounts);
//        stage.show();
    }
    public void handleBackBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        MainController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        AdminChangePasswordController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
}
