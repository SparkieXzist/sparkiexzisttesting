package services;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class UserMainPageController
{
    private AdminAccount adminAccount;
    private HostAccount hostAccount;
    private UserAccount userAccount;
    private Room roomAll;
    public void setRoom(Room room) {roomAll = room;}
    public void setAdminAccounts(AdminAccount accounts) { adminAccount = accounts; }
    public void setHostAccounts(HostAccount accounts) { hostAccount = accounts; }
    public void setUserAccounts(UserAccount accounts) { userAccount = accounts;}
    @FXML private Label currentAccountLabel;
    @FXML
    public void initialize()
    {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccountLabel.setText(userAccount.getCurrentAccount().getName());
            }
        });
    }
    public void handleBackBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        MainController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/userchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserChangePasswordController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
}
