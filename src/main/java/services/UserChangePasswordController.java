package services;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;

import java.io.IOException;

public class UserChangePasswordController
{

    @FXML PasswordField oldPasswordBox,newPasswordBox,newPasswordConfirmBox;
    @FXML Label currentAccountLabel;
    private AdminAccount adminAccount;
    private HostAccount hostAccount;
    private UserAccount userAccount;
    private Room roomAll;
    public void setRoom(Room room) {roomAll = room;}
    public void setAdminAccounts(AdminAccount accounts) { adminAccount = accounts; }
    public void setHostAccounts(HostAccount accounts) { hostAccount = accounts; }
    public void setUserAccounts(UserAccount accounts) { userAccount = accounts;}
    Alert alert;
    public void initialize()
    {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccountLabel.setText(userAccount.getCurrentAccount().getName());
            }
        });
        alert = new Alert(Alert.AlertType.NONE);
    }

    public void handleBackBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/usermainpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserMainPageController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }

    public void handleChangeBtnOnAction(ActionEvent event) throws IOException
    {
        if(userAccount.getCurrentAccount().getPassword().equals(oldPasswordBox.getText()) && newPasswordBox.getText().equals(newPasswordConfirmBox.getText()))
        {
            userAccount.getCurrentAccount().setPassword(newPasswordBox.getText());
            alert.setAlertType(Alert.AlertType.INFORMATION);
            alert.setContentText("Change password successful.");
            alert.show();
            Button LoginBtn = (Button) event.getSource();
            Stage stage = (Stage) LoginBtn.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/userchangepassword.fxml"));
            stage.setScene(new Scene(loader.load(), 1024, 768));
            UserChangePasswordController account = loader.getController();
            account.setAdminAccounts(adminAccount);
            account.setHostAccounts(hostAccount);
            account.setUserAccounts(userAccount);
            account.setRoom(roomAll);
            stage.show();
        }
        else if(!newPasswordBox.getText().equals(newPasswordConfirmBox))
        {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Password doesn't match.");
            alert.show();
        }
    }
    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/userchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        UserChangePasswordController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
}
