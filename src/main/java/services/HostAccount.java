package services;

import java.util.ArrayList;

public class HostAccount extends Account
{
    private ArrayList<HostAccount> hostAccounts;
    private int attempt;
    public HostAccount(String username, String password, String name, int status)
    {
        super(username, password, name, status);
        hostAccounts = new ArrayList<>();
        attempt = 0;
    }

    public void addAccount(HostAccount acc){ hostAccounts.add(acc);}
    public boolean checkUsernamePassword(String username, String password)
    {
        System.out.println("Host Account");
        System.out.println(hostAccounts);
        for(HostAccount acc: hostAccounts)
        {
            if(acc.getUsername().equals(username) && acc.getPassword().equals(password))
            {
                setCurrentAccount(acc);
                return true;
            }
        }
        setCurrentAccount(null);
        return false;
    }
    public boolean checkExist(String username)
    {
        for(HostAccount acc: hostAccounts)
        {
            if(acc.getUsername().equals(username))
            {
                return true;
            }
        }
        return false;
    }
    public int getAttempt() { return attempt; }

    public void setAttempt(int value)
    {
        if(value == 1)
            attempt += 1;
        else
            attempt = 0;
    }

//    public void setAttempt() {
//        attempt += 1;
//    }
//    public int getAttempt() { return attempt; }

    @Override
    public String toString() { return "Host: "+getUsername()+" Password: "+getPassword()+" Status: "+getStatus()+" Attempt: "+getAttempt();}
    public ArrayList<HostAccount> getHostAccounts() { return hostAccounts; }
}
