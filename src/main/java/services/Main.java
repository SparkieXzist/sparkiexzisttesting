package services;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application
{
    private AdminAccount adminAccount;
    private HostAccount hostAccount;
    private UserAccount userAccount;
    public void setAdminAccounts(AdminAccount accounts) { adminAccount = accounts; }
    public void setHostAccounts(HostAccount accounts) { hostAccount = accounts; }
    public void setUserAccounts(UserAccount accounts) { userAccount = accounts; }
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Parent root = FXMLLoader.load(getClass().getResource("/main.fxml"));
        primaryStage.setTitle("Sparkie dormitory");
        primaryStage.setScene(new Scene(root, 1024, 768));
        primaryStage.setResizable(false);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}
