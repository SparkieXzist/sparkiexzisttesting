package services;

import java.util.ArrayList;

public class Account
{
    private String username;
    private String password;
    private String name;
    private Account currentAccount;
    private int status;

    public Account(String username, String password, String name,int status)
    {
        this.username = username;
        this.password = password;
        this.name = name;
        this.status = 1;
    }
    public String getUsername() { return username; }
    public String getPassword() { return password; }
    public String getName() { return name; }

    public void setPassword(String password) { this.password = password; }

    public Account getCurrentAccount() { return currentAccount; }

    public int getStatus() { return status; }

    public void setStatus(int status) { this.status = status; }

    public void setCurrentAccount(Account currentAccount) { this.currentAccount = currentAccount; }
    @Override
    public String toString() {
        return "Username: "+username+" Password: "+password+" Status: "+status;}
}
