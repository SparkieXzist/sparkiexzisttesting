package services;

import java.util.ArrayList;

public class UserAccount extends Account
{
    private ArrayList<UserAccount> userAccounts;
    private String room;
    public UserAccount(String username, String password, String name,String room, int status)
    {
        super(username, password, name, status);
        userAccounts = new ArrayList<>();
        this.room = room;
    }
    public boolean checkUsernamePassword(String username, String password)
    {
        System.out.println("User Account");
        System.out.println(userAccounts);
        for(UserAccount acc: userAccounts)
        {
            if(acc.getUsername().equals(username) && acc.getPassword().equals(password))
            {
                setCurrentAccount(acc);
                return true;
            }
        }
        setCurrentAccount(null);
        return false;
    }
    public boolean checkExist(String username)
    {
        for(UserAccount acc: userAccounts)
        {
            if(acc.getUsername().equals(username))
            {
                return true;
            }
        }
        return false;
    }
    public void addAccount(UserAccount acc){ userAccounts.add(acc);}
    public ArrayList<UserAccount> getUserAccounts() { return userAccounts; }
}
