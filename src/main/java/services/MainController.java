package services;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.control.Alert;

import java.io.IOException;

public class MainController
{
    @FXML TextField usernameBox;
    @FXML PasswordField passwordBox;
    private AdminAccount adminAccount;
    private HostAccount hostAccount;
    private UserAccount userAccount;
    private Room roomAll;
    public void setRoom(Room room) {roomAll = room;}
    public void setAdminAccounts(AdminAccount accounts) { adminAccount = accounts; }
    public void setHostAccounts(HostAccount accounts) { hostAccount = accounts; }
    public void setUserAccounts(UserAccount accounts) { userAccount = accounts;}
    Alert alert;
    public void initialize()
    {
        adminAccount = new AdminAccount("1","1","Teeraphat Sutparsoet",1);
        adminAccount.firstAdmin();
        hostAccount = new HostAccount("2","2","Sparkie Lauv",1);
        userAccount = new UserAccount("3","3","Xzist","000",1);
        roomAll = new Room(999,"system","system",0);
        alert = new Alert(Alert.AlertType.NONE);
    }
    public void handleLoginBtnOnAction(ActionEvent event) throws IOException
    {
//        for(HostAccount acc: hostAccount.getHostAccounts())
//            System.out.println(acc);
        if(adminAccount.checkUsernamePassword(usernameBox.getText(),passwordBox.getText()))
        {
            Button LoginBtn = (Button) event.getSource();
            Stage stage = (Stage) LoginBtn.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/adminmainpage.fxml"));
            stage.setScene(new Scene(loader.load(), 1024, 768));
            AdminMainPageController account = loader.getController();
            account.setAdminAccounts(adminAccount);
            account.setHostAccounts(hostAccount);
            account.setUserAccounts(userAccount);
            account.setRoom(roomAll);
            stage.show();
        }
        else if(hostAccount.checkUsernamePassword(usernameBox.getText(),passwordBox.getText()) && hostAccount.getCurrentAccount().getStatus() == 1 )
        {
            Button LoginBtn = (Button) event.getSource();
            Stage stage = (Stage) LoginBtn.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostmainpage.fxml"));
            stage.setScene(new Scene(loader.load(), 1024, 768));
            HostMainPageController account = loader.getController();
            account.setAdminAccounts(adminAccount);
            account.setHostAccounts(hostAccount);
            account.setUserAccounts(userAccount);
            account.setRoom(roomAll);
            stage.show();
        }
        else if(hostAccount.checkUsernamePassword(usernameBox.getText(),passwordBox.getText()) && hostAccount.getCurrentAccount().getStatus() == 0)
        {
            ((HostAccount)hostAccount.getCurrentAccount()).setAttempt(1);
        }
        else if(userAccount.checkUsernamePassword(usernameBox.getText(),passwordBox.getText()))
        {
            Button LoginBtn = (Button) event.getSource();
            Stage stage = (Stage) LoginBtn.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/usermainpage.fxml"));
            stage.setScene(new Scene(loader.load(), 1024, 768));
            UserMainPageController account = loader.getController();
            account.setAdminAccounts(adminAccount);
            account.setHostAccounts(hostAccount);
            account.setUserAccounts(userAccount);
            account.setRoom(roomAll);
            stage.show();
        }
        else
        {
            alert.setAlertType(Alert.AlertType.WARNING);
            alert.setContentText("Username or password incorrect.");
            alert.show();
        }
    }
}
