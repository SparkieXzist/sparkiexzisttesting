package services;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class ManageRoomController
{
    @FXML private Label currentAccountLabel;
    private AdminAccount adminAccount;
    private HostAccount hostAccount;
    private UserAccount userAccount;
    private Room roomAll;
    @FXML private TextField roomBox, sizeBox, addUserRoomBox, addUserNameBox;
    public void setRoom(Room room) {roomAll = room;}
    public void setAdminAccounts(AdminAccount accounts) { adminAccount = accounts; }
    public void setHostAccounts(HostAccount accounts) { hostAccount = accounts; }
    public void setUserAccounts(UserAccount accounts) { userAccount = accounts;}

    public void initialize()
    {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccountLabel.setText(hostAccount.getCurrentAccount().getName());
            }
        });
    }

    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostChangePasswordController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
    public void handleManageUserBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageuserpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageUserPageController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
    public void handleManageRoomBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageroom.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageRoomController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
    public void handleBackBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        MainController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
    public void handleAddBtnOnAction(ActionEvent event) throws IOException
    {
        try
        {
            Integer.parseInt(roomBox.getText());
            if(!roomAll.checkExist(Integer.parseInt(roomBox.getText())))
            {
                if (sizeBox.getText().equals("4")) {
                    roomAll.addRoom(new Room(Integer.parseInt(roomBox.getText()), "free", "free", "free", "free", 4));
                    roomAll.printCheck();
                } else if (sizeBox.getText().equals("2")) {
                    roomAll.addRoom(new Room(Integer.parseInt(roomBox.getText()), "free", "free", 4));
                    roomAll.printCheck();
                }
            }
            else
            {
                System.out.println("Room already exist.");
            }
        }
        catch (NumberFormatException e)
        {
            System.out.println("Error");
        }
//        if(Integer.parseInt(roomBox.getText()))
//        {
//            roomAll.addRoom(new Room(Integer.parseInt(roomBox.getText()), "free", "free", "free", "free", 4));
//            roomAll.printCheck();
//        }
//        else
//        {
//            System.out.println("Error");
//        }
    }
    public void handleAddToRoomBtnOnAction(ActionEvent event) throws IOException
    {
        if(userAccount.checkExist(addUserNameBox.getText()))
        {
            roomAll.setCustomer(addUserNameBox.getText(), Integer.parseInt(addUserRoomBox.getText()));
            roomAll.printCheck();
        }
        else
        {
            System.out.println("Name doesn't exist.");
        }
    }
}
