package services;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;

import java.io.IOException;

public class HostChangePasswordController
{
    Alert alert;
    @FXML
    private Label currentAccountLabel;
    @FXML public void initialize()
    {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentAccountLabel.setText(hostAccount.getCurrentAccount().getName());
            }
        });
        alert = new Alert(Alert.AlertType.NONE);
    }
    @FXML
    PasswordField oldPasswordBox,newPasswordBox,newPasswordConfirmBox;
    private AdminAccount adminAccount;
    private HostAccount hostAccount;
    private UserAccount userAccount;
    private Room roomAll;
    public void setRoom(Room room) {roomAll = room;}
    public void setAdminAccounts(AdminAccount accounts) { adminAccount = accounts; }
    public void setHostAccounts(HostAccount accounts) { hostAccount = accounts; }
    public void setUserAccounts(UserAccount accounts) { userAccount = accounts;}

    public void handleChangeBtnOnAction(ActionEvent event) throws IOException
    {
        if(hostAccount.getCurrentAccount().getPassword().equals(oldPasswordBox.getText()) && newPasswordBox.getText().equals(newPasswordConfirmBox.getText()))
        {
            hostAccount.getCurrentAccount().setPassword(newPasswordBox.getText());
            alert.setAlertType(Alert.AlertType.INFORMATION);
            alert.setContentText("Change password successful.");
            alert.show();
            Button LoginBtn = (Button) event.getSource();
            Stage stage = (Stage) LoginBtn.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostchangepassword.fxml"));
            stage.setScene(new Scene(loader.load(), 1024, 768));
            HostChangePasswordController account = loader.getController();
            account.setAdminAccounts(adminAccount);
            account.setHostAccounts(hostAccount);
            account.setUserAccounts(userAccount);
            account.setRoom(roomAll);
            stage.show();
        }
    }
    public void handleChangePasswordBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/hostchangepassword.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        HostChangePasswordController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }

    public void handleManageUserBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageuserpage.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageUserPageController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
    public void handleHostListBtnOnAction(ActionEvent event) throws IOException
    {
//        System.out.println(accounts.getCurrentAccount());
//        Button LoginBtn = (Button) event.getSource();
//        Stage stage = (Stage) LoginBtn.getScene().getWindow();
//        FXMLLoader loader = new FXMLLoader(getClass().getResource("managehostpage.fxml"));
//        stage.setScene(new Scene(loader.load(), 1024, 768));
//        ManageHostPageController account = loader.getController();
//        account.setAccounts(accounts);
//        stage.show();
    }
    public void handleBackBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        MainController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }
    public void handleManageRoomBtnOnAction(ActionEvent event) throws IOException
    {
        Button LoginBtn = (Button) event.getSource();
        Stage stage = (Stage) LoginBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/manageroom.fxml"));
        stage.setScene(new Scene(loader.load(), 1024, 768));
        ManageRoomController account = loader.getController();
        account.setAdminAccounts(adminAccount);
        account.setHostAccounts(hostAccount);
        account.setUserAccounts(userAccount);
        account.setRoom(roomAll);
        stage.show();
    }

}
