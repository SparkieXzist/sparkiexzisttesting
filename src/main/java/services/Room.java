package services;

import java.util.ArrayList;

public class Room
{
    private ArrayList<Room> allRooms;
    private int roomNumber;
    private String customer1;
    private String customer2;
    private String customer3;
    private String customer4;
    private int amount;
    private String size;

    public Room(int roomNumber, String customer1, String customer2, String customer3,String customer4,int amount)
    {
        this.roomNumber = roomNumber;
        this.customer1 = customer1;
        this.customer2 = customer2;
        this.customer3 = customer3;
        this.customer4 = customer4;
        this.amount = 4;
        this.size = "Double room";
        allRooms = new ArrayList<>();
    }
    public Room(int roomNumber, String customer1, String customer2, int amount)
    {
        this.roomNumber = roomNumber;
        this.customer1 = customer1;
        this.customer2 = customer2;
        customer3 = "Not available";
        customer4 = "Not available";
        this.amount = 2;
        this.size = "Single room";
        allRooms = new ArrayList<>();
    }

    public void addRoom(Room room) { allRooms.add(room);}

    public int getRoomNumber() {
        return roomNumber;
    }

    public String getCustomer1() {
        return customer1;
    }

    public String getCustomer2() {
        return customer2;
    }

    public String getCustomer3() {
        return customer3;
    }

    public String getCustomer4() {
        return customer4;
    }

    public int getAmount() {
        return amount;
    }

    public String getSize() {
        return size;
    }

    public void setCustomer1(String customer1) {
        this.customer1 = customer1;
    }

    public void setCustomer2(String customer2) {
        this.customer2 = customer2;
    }

    public void setCustomer3(String customer3) {
        this.customer3 = customer3;
    }

    public void setCustomer4(String customer4) {
        this.customer4 = customer4;
    }

    public boolean setCustomer(String name,int roomNumber)
    {
        for(Room room: allRooms)
        {
            if(room.getAmount() == 2 && room.getRoomNumber() == roomNumber )
            {
                if(room.getCustomer1().equals("free"))
                {
                    room.setCustomer1(name);
                    return true;
                }
                else if(room.getCustomer2().equals("free"))
                {
                    room.setCustomer2(name);
                    return true;
                }
            }
            else if(room.getAmount() == 4 && room.getRoomNumber() == roomNumber)
            {
                if(room.getCustomer1().equals("free"))
                {
                    room.setCustomer1(name);
                    return true;
                }
                else if(room.getCustomer2().equals("free"))
                {
                    room.setCustomer2(name);
                    return true;
                }
                else if(room.getCustomer3().equals("free"))
                {
                    room.setCustomer3(name);
                    return true;
                }
                else if(room.getCustomer4().equals("free"))
                {
                    room.setCustomer4(name);
                    return true;
                }
            }
        }
        return false;
    }
    public void printCheck()
    {
        for(Room room: allRooms)
        {
            System.out.println(room);
        }
    }
    public boolean checkExist(int roomNumber)
    {
        for(Room room: allRooms)
        {
            if(room.getRoomNumber() == roomNumber)
            {
                return true;
            }
        }
        return false;
    }
    @Override
    public String toString() {
        return "Room{" +
                "allRooms=" + allRooms +
                ", roomNumber='" + roomNumber + '\'' +
                ", customer1='" + customer1 + '\'' +
                ", customer2='" + customer2 + '\'' +
                ", customer3='" + customer3 + '\'' +
                ", customer4='" + customer4 + '\'' +
                ", amount=" + amount +
                ", size='" + size + '\'' +
                '}';
    }
}
