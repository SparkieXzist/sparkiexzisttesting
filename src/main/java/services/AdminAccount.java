package services;

import java.util.ArrayList;

public class AdminAccount extends Account
{
    private ArrayList<AdminAccount> adminAccounts;
    private AdminAccount currentAccount;

    public AdminAccount(String username, String password, String name, int status)
    {
        super(username, password, name, status);
        adminAccounts = new ArrayList<>();
    }
//    public void addHostAccount(HostAccount acc){ host.addAccount(acc);}
//
//    public ArrayList<HostAccount> getHostList()
//    {
//        return host.getHostAccounts();
//    }
    public boolean checkUsernamePassword(String username, String password) {
        System.out.println("Admin Account");
        System.out.println(adminAccounts);
        for (AdminAccount acc : adminAccounts) {
            if (acc.getUsername().equals(username) && acc.getPassword().equals(password)) {
                setCurrentAccount(acc);
                return true;
            }
        }
        setCurrentAccount(null);
        return false;
    }
    @Override
    public String toString() { return "Admin: "+getUsername()+" Password: "+getPassword()+" Status: "+getStatus();}
    public void firstAdmin(){ adminAccounts.add(new AdminAccount("1","1","Teeraphat Sutprasoet", 1));}

    public ArrayList<AdminAccount> getAdminAccounts() { return adminAccounts; }
}
